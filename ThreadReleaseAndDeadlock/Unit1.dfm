object Form1: TForm1
  Left = 294
  Top = 241
  Caption = 'Form1'
  ClientHeight = 300
  ClientWidth = 701
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object mmo1: TMemo
    Left = 8
    Top = 8
    Width = 433
    Height = 289
    Lines.Strings = (
      'mmo1')
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object btnThreadDoNothingButSleep: TButton
    Left = 448
    Top = 8
    Width = 241
    Height = 25
    Action = actThreadDoNothingButSleep
    TabOrder = 1
  end
  object btnThreadSendCopyData: TButton
    Left = 448
    Top = 40
    Width = 241
    Height = 25
    Action = actThreadSendCopyData
    TabOrder = 2
  end
  object actlst1: TActionList
    Left = 312
    Top = 72
    object actThreadDoNothingButSleep: TAction
      Caption = 'actThreadDoNothingButSleep'
      OnExecute = actThreadDoNothingButSleepExecute
    end
    object actThreadSendCopyData: TAction
      Caption = 'actThreadSendCopyData'
      OnExecute = actThreadSendCopyDataExecute
    end
  end
end
