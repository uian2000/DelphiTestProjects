unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList;

type
  TForm1 = class(TForm)
    mmo1: TMemo;
    btnThreadDoNothingButSleep: TButton;
    actlst1: TActionList;
    actThreadDoNothingButSleep: TAction;
    btnThreadSendCopyData: TButton;
    actThreadSendCopyData: TAction;
    procedure actThreadDoNothingButSleepExecute(Sender: TObject);
    procedure actThreadSendCopyDataExecute(Sender: TObject);
  private
    { Private declarations }
    procedure WMCOPYDATA(var Msg: TWMCopyData); Message WM_COPYDATA;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
  uThreads;

var
  oThreadDoNothingButSleep: TThreadDoNothingButSleep;
  oThreadSendCopyData: TThreadSendsCopyData;

procedure TForm1.actThreadDoNothingButSleepExecute(Sender: TObject);
begin
  if nil = oThreadDoNothingButSleep then
  begin
    oThreadDoNothingButSleep := TThreadDoNothingButSleep.Create;
    oThreadDoNothingButSleep.Resume;
    mmo1.Lines.Add(oThreadDoNothingButSleep.ClassName + ' has been created');
  end else
  begin
    mmo1.Lines.Add(oThreadDoNothingButSleep.ClassName + ' will been destroyed');
    FreeAndNil(oThreadDoNothingButSleep);
  end;
end;

procedure TForm1.actThreadSendCopyDataExecute(Sender: TObject);
begin
  if nil = oThreadSendCopyData then
  begin
    oThreadSendCopyData := TThreadSendsCopyData.Create(Handle);
    oThreadSendCopyData.Resume;
    mmo1.Lines.Add(oThreadSendCopyData.ClassName + ' has been created');
  end else
  begin
    mmo1.Lines.Add(oThreadSendCopyData.ClassName + ' will been destroyed');
    FreeAndNil(oThreadSendCopyData);
  end;
end;

procedure TForm1.WMCOPYDATA(var Msg: TWMCopyData);
var
  S: string;
begin
  if Msg.CopyDataStruct^.cbData <= 0 then Exit;
  SetLength(S, Msg.CopyDataStruct^.cbData);
  Move(Msg.CopyDataStruct^.lpData^, S[1], Msg.CopyDataStruct^.cbData);
  mmo1.Lines.Add(S);
end;

end.
 